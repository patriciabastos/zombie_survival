module API
  module V1
    class ReportsController < APIController

      def index
        render json: {
          percentage_survivors: {
            percentage: Services::Report.percentage_survivors
          },
          percentage_non_infected: {
            percentage: Services::Report.percentage_non_survivors
          },
          average_amount_items_by_survivor: {
            water: Services::Report.water_by_survivor,
            food: Services::Report.food_by_survivor,
            medication: Services::Report.medication_by_survivor,
            ammunition: Services::Report.ammunition_by_survivor
          },
          points_lost_of_infected_survivor: {
            lost_points: Services::Report.lost_points
          }
        }
      end

      def percentage_survivors
        render json: {
          percentage_survivors: Services::Report.percentage_survivors
        }
      end

      def percentage_non_infected
        render json: {
          percentage_non_infected: Services::Report.percentage_non_survivors
        }
      end

      def average_amount_items_by_survivor
        render json: {
          water: Services::Report.water_by_survivor,
          food: Services::Report.food_by_survivor,
          medication: Services::Report.medication_by_survivor,
          ammunition: Services::Report.ammunition_by_survivor
        }
      end

      def points_lost_of_infected_survivor
        render json: {
          lost_points: Services::Report.lost_points
        }
      end
    end
  end
end