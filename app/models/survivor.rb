# == Schema Information
#
# Table name: survivors
#
#  id                    :integer          not null, primary key
#  name                  :string
#  age                   :integer
#  gender                :string(1)
#  lat                   :string
#  long                  :string
#  infection_alarm_count :integer          default("0")
#  created_at            :datetime
#  updated_at            :datetime
#

class Survivor  < ActiveRecord::Base

  has_many :inventories

  validates :name, :age, :gender, :lat, :long, presence: :true
  validates :infection_alarm_count, numericality: { only_integer: true, greater_than_or_equal_to: 0}
  validates :gender, format: { with: /\A[m|f]\z/,
                               message: I18n.t('.errors.messages.invalid', resource: Survivor.human_attribute_name(:gender)) }

  scope :infected, -> {  where('infection_alarm_count >= 3')}
  scope :non_infected, -> {  where('infection_alarm_count < 3')}

  after_create :build_inventory

  def infected?
    infection_alarm_count >= 3
  end

  def build_inventory
    Inventory::ITEMS.each do |item, _|
      self.inventories.create item: item
    end
  end
end
