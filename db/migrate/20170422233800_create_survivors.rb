class CreateSurvivors < ActiveRecord::Migration
  def up
    create_table :survivors do |t|
      t.string :name
      t.integer :age
      t.string :gender, limit: 1
      t.string :lat
      t.string :long
      t.integer :infection_alarm_count, default: 0

      t.timestamps
    end
  end

  def down
    drop_table :survivors
  end
end