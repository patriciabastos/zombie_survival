# == Schema Information
#
# Table name: survivors
#
#  id                    :integer          not null, primary key
#  name                  :string
#  age                   :integer
#  gender                :string(1)
#  lat                   :string
#  long                  :string
#  infection_alarm_count :integer          default("0")
#  created_at            :datetime
#  updated_at            :datetime
#

require 'rails_helper'

RSpec.describe Survivor, type: :model do
  context "Validations" do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_presence_of :age }
    it { is_expected.to validate_presence_of :gender }
    it { is_expected.to validate_presence_of :lat }
    it { is_expected.to validate_presence_of :long }
    it { is_expected.to validate_numericality_of(:infection_alarm_count).only_integer.is_greater_than_or_equal_to 0 }
  end

  context "Relationships" do
    it { should have_many :inventories }
  end

  context "Scopes" do
    let!(:survivor_infected) { create :survivor_infected }
    before { create_list :survivor, 2 }
    context 'infected' do
      it { expect(Survivor.infected.count).to be(1) }
    end

    context '.non_infected' do
      it { expect(Survivor.non_infected.count).to be(2) }
    end
  end

  context "survivor has been infected" do
    let(:survivor_infected) { create :survivor_infected }
    it "infection_alarm_count is 3" do
      expect(survivor_infected.infection_alarm_count).to be >= 3
    end

    it { expect(survivor_infected).to be_infected }
  end
end
