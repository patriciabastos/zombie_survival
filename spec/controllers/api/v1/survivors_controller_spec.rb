require 'rails_helper'

RSpec.describe API::V1::SurvivorsController, type: :controller do
  let(:survivor) { create :survivor }

  it { should use_before_filter(:find_survivor) }

  describe '#create' do
    context 'with valid parameters' do
      let(:values) do
        {
          name: 'Negan',
          age: 45,
          gender: 'm',
          lat: 38.8951100,
          long: -77.0363700
        }
      end

      before do
        post :create, survivor: values
        @json_response = JSON.parse(response.body).with_indifferent_access
      end

      it 'creates a new survivor' do
        expect(response).to have_http_status(:success)
      end

      it 'have a valid id'  do
        expect(@json_response[:survivor][:id]).to be_a(Numeric)
      end


      it 'have default items'  do
        inventories = @json_response[:survivor][:inventories].map {|inventory| inventory[:item].to_sym}
        expect(inventories).to match(Inventory::ITEMS.keys)
      end

      it 'return survivor keys' do
        expect(@json_response[:survivor].keys)
          .to eq(%w[id name age gender lat long infected inventories ])
      end
    end

    context 'with invalid parameters' do
      let(:values) do
        {
          age: 47,
          gender: 'm',
          lat: 38.8951100,
          long: -77.0363700
        }
      end

      before do
        post :create, survivor: values
        @json_response = JSON.parse(response.body).with_indifferent_access
      end

      it 'returns status 422' do
        post :create, params: { survivor: values }

        expect(response.status).to eq(422)
      end

      it 'expect error message error' do
        expect(@json_response[:name]).to eq(["can't be blank"])
      end

    end
  end

  describe '#index' do
    context 'with collection' do
      before do
        create_list :survivor, 2
        get :index
        @json_response = JSON.parse(response.body).with_indifferent_access
      end

      it "returns http success" do
        expect(response).to have_http_status(:success)
      end

      it "an list of survivor" do
        expect(@json_response[:survivors].size).to be(2)
      end
    end

    context 'without collection' do
      before do
        get :index
        @json_response = JSON.parse(response.body).with_indifferent_access
      end

      it "returns http success" do
        expect(response).to have_http_status(:success)
      end

      it "an list of survivor" do
        expect(@json_response[:survivors].size).to be(0)
      end
    end
  end

  describe '#update' do
    context 'with valid parameters' do
      let(:values) do
        {
          lat: 38.8951100,
          long: -77.0363700
        }
      end

      it 'update survivor' do
        response = post :update, id: survivor.id, survivor: values
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe "#alert_infection" do
    context 'when survivor has been infected' do
      it do
        response = post "alert_infection", id: survivor.id
        response = JSON.parse(response.body).with_indifferent_access
        expect(response[:message]).to eq("Survivor reported as infected #{survivor.infection_alarm_count + 1} times")
      end
    end

    context 'when survivor will infected' do
      before do
        survivor.infection_alarm_count = 2
        survivor.save
      end
      it do
        response = post "alert_infection", id: survivor.id
        response = JSON.parse(response.body).with_indifferent_access
        expect(response[:message]).to eq("Infected survivor!")
      end
    end
  end

  describe '#trade' do
    let(:survivor_required) { create :survivor }
    let(:survivor_offering) { create :survivor }
    context 'with valid parameters' do
      let(:trade_required) do
        {
          items: '["water", "ammunition"]'
        }
      end

      let(:trade_offering) do
        {
          items: '["food", "medication"]',
          id: survivor_offering.id
        }
      end

      it 'returns http success' do
        response = post "trade", id: survivor_required.id, trade_required: trade_required, trade_offering: trade_offering
        expect(response).to have_http_status(:success)
      end
    end

    context 'with not acceptable parameters' do
      let(:trade_required) do
        {
          items: '["water", "food"]'
        }
      end

      let(:trade_offering) do
        {
          items: '["food", "medication"]',
          id: survivor_offering.id
        }
      end

      it 'returns http success' do
        response = post "trade", id: survivor_required.id, trade_required: trade_required, trade_offering: trade_offering
        expect(response).to have_http_status(:not_acceptable)
      end
    end
  end
end