module API
  module V1
    class SurvivorsController < APIController
      before_filter :find_survivor, only: [:show, :update, :alert_infection, :trade]
      before_filter :find_offering_survivor, only: [:trade]

      def index
        survivors = Survivor.all.order(:id)
        render json: survivors
      end

      def create
        @survivor = Survivor.new survivor_params
        if @survivor.save
          render json: @survivor, status: :created
        else
          render json: @survivor.errors, status: :unprocessable_entity
        end
      end

      def show
        render json: @survivor
      end

      def update
        if @survivor.update survivor_params
          render json: @survivor, status: :ok
        else
          render json: @survivor.errors, status: :unprocessable_entity
        end
      end

      def alert_infection
        @survivor.increment :infection_alarm_count, 1
        @survivor.save
        if @survivor.infected?
          render json: { message: "Infected survivor!" },
                 status: :ok
        else
          render json: { message: "Survivor reported as infected #{@survivor.infection_alarm_count} times" },
                 status: :ok
        end
      end

      def trade
        trade = Services::Trade.new(@survivor, required_params[:items], @offering_survivor, offer_params[:items])
        if trade.process
          render json: { message: "Success trading" },
                 status: :ok
        else
          render json: { message: "Not permited!" },
                 status: :not_acceptable
        end
      end

      private

      def find_survivor
        @survivor = Survivor.find params[:id]
      end

      def find_offering_survivor
        @offering_survivor = Survivor.find params[:trade_offering][:id]
      end

      def survivor_params
        params.require(:survivor).permit :name, :age, :gender, :lat, :long
      end

      def required_params
        params.require(:trade_required).permit :items
      end

      def offer_params
        params.require(:trade_offering).permit :items, :id
      end
    end
  end
end
