Rails.application.routes.draw do
  resources :survivors, :inventories

  namespace :api, format: :json do
    namespace :v1 do
      resources :survivors, except: [:destroy] do
        post :alert_infection, on: :member
        post :trade, on: :member
      end
      resources :reports, only: [:index] do
        get :percentage_survivors, on: :collection
        get :percentage_non_infected, on: :collection
        get :average_amount_items_by_survivor, on: :collection
        get :points_lost_of_infected_survivor, on: :collection
      end
    end
  end
end
