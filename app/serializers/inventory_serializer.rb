class InventorySerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers

  attributes :id, :item, :point

  def point
    Inventory::ITEMS[object.item.to_sym]
  end
end
