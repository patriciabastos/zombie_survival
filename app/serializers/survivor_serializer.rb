class SurvivorSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers

  attributes :id, :name, :age, :gender,
             :lat, :long,
             :infected,
             :inventories

  has_many :inventories

  def infected
    object.infected?
  end
end