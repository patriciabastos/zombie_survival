class Services::Trade
  require 'json'

  def initialize(survivor, required_items, offering_survivor, offering_items)
    @survivor = survivor
    @offering_survivor = offering_survivor
    @required_items = JSON.parse required_items
    @offering_items = JSON.parse offering_items
  end

  def process
    return if check_infection? || !fair_trade?
    trading
  end

  def check_infection?
    @survivor.infected? || @offering_survivor.infected?
  end

  def fair_trade?
    check_points_items(@offering_items) == check_points_items(@required_items)
  end

  def check_points_items(items)
    items.inject(0) { |sum, item|  sum + Inventory::ITEMS[item.to_sym]   }
  end

  def trading
    @required_items.map{|item| @survivor.inventories
                                .where(item: item)
                                .update_all(survivor_id: @offering_survivor.id) }

    @offering_items.map{|item| @offering_survivor.inventories
                                .where(item: item)
                                .update_all(survivor_id: @survivor.id) }
  end
end