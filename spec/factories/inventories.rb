# == Schema Information
#
# Table name: inventories
#
#  id          :integer          not null, primary key
#  item        :string
#  survivor_id :integer          not null
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_inventories_on_survivor_id  (survivor_id)
#

FactoryGirl.define do
  factory :inventory do
    item nil
    survivor nil
  end
end
