# == Schema Information
#
# Table name: survivors
#
#  id                    :integer          not null, primary key
#  name                  :string
#  age                   :integer
#  gender                :string(1)
#  lat                   :string
#  long                  :string
#  infection_alarm_count :integer          default("0")
#  created_at            :datetime
#  updated_at            :datetime
#

FactoryGirl.define do
  factory :survivor do
    name { Faker::Name.name }
    age { Faker::Number.number(2) }
    gender 'f'
    lat { Faker::Number.decimal(2, 6) }
    long { Faker::Number.decimal(2, 6) }

    factory :survivor_infected do
      infection_alarm_count 3
    end
  end
end
