# zombie_survival

### API

#### GET Survivor - index
/api/v1/survivors


#Response
{
  "survivors": [
    {
      "id": 15,
      "name": "Rick Grimes",
      "age": 40,
      "gender": "m",
      "lat": "-22.8546496",
      "long": "-47.0541292",
      "infected": false,
      "inventories": [
        {
          "id": 45,
          "item": "ammunition",
          "point": 1
        },
        {
          "id": 44,
          "item": "medication",
          "point": 2
        },
        {
          "id": 43,
          "item": "food",
          "point": 3
        },
        {
          "id": 42,
          "item": "water",
          "point": 4
        }
      ]
    },
    {
      "id": 16,
      "name": "Michonne",
      "age": 35,
      "gender": "f",
      "lat": "-22.8546496",
      "long": "-47.0541292",
      "infected": false,
      "inventories": [
        {
          "id": 49,
          "item": "ammunition",
          "point": 1
        },
        {
          "id": 48,
          "item": "medication",
          "point": 2
        },
        {
          "id": 47,
          "item": "food",
          "point": 3
        },
        {
          "id": 46,
          "item": "water",
          "point": 4
        }
      ]
    }
  ]
}

#### POST Survivor - create
/api/v1/survivors

#Body
curl --request POST \
  --url http://localhost:3000//api/v1/survivors \
  --header 'content-type: multipart/form-data; boundary=---011000010111000001101001' \
  --form 'survivor[name]=Rick Grimes' \
  --form 'survivor[age]=40' \
  --form 'survivor[gender]=m' \
  --form 'survivor[lat]=-22.8546496' \
  --form 'survivor[long]=-47.0541292'


#Response
{
  "survivor": {
    "id": 15,
    "name": "Rick Grimes",
    "age": 40,
    "gender": "m",
    "lat": "-22.8546496",
    "long": "-47.0541292",
    "infected": false,
    "inventories": [
      {
        "id": 45,
        "item": "ammunition",
        "point": 1
      },
      {
        "id": 44,
        "item": "medication",
        "point": 2
      },
      {
        "id": 43,
        "item": "food",
        "point": 3
      },
      {
        "id": 42,
        "item": "water",
        "point": 4
      }
    ]
  }
}

#### PUT Survivor - update survivor
/api/v1/survivors/15

#Body
curl --request PUT \
  --url http://localhost:3000//api/v1/survivors/11 \
  --header 'content-type: multipart/form-data; boundary=---011000010111000001101001' \
  --form 'survivor[lat]=-20.8546496' \
  --form 'survivor[long]=-45.0541292'

#Response
{
  "survivor": {
    "id": 15,
    "name": "Rick Grimes",
    "age": 40,
    "gender": "m",
    "lat": "-20.8546496",
    "long": "-45.0541292",
    "infected": false,
    "inventories": [
      {
        "id": 42,
        "item": "water",
        "point": 4
      },
      {
        "id": 43,
        "item": "food",
        "point": 3
      },
      {
        "id": 44,
        "item": "medication",
        "point": 2
      },
      {
        "id": 45,
        "item": "ammunition",
        "point": 1
      }
    ]
  }
}

#### POST Report - alert_infection
/api/v1/survivors/15/alert_infection

#Response
{
  "message": "Infected survivor!"
}

OR

{
  "message": "Survivor reported as infected 1 times"
}

#### POST Survivor - trade
/api/v1/survivors/15/trade

#Body success
curl --request POST \
  --url http://localhost:3000//api/v1/survivors/15/trade \
  --header 'content-type: multipart/form-data; boundary=---011000010111000001101001' \
  --form 'trade_required[items]=["water", "ammunition"]' \
  --form 'trade_offering[items]=["food", "medication"]' \
  --form 'trade_offering[id]=16'

#Response
{
  "message": "Success trading"
}

#Body not acceptable
curl --request POST \
  --url http://localhost:3000//api/v1/survivors/15/trade \
  --header 'content-type: multipart/form-data; boundary=---011000010111000001101001' \
  --form 'trade_required[items]=["water", "food"]' \
  --form 'trade_offering[items]=["food", "medication"]' \
  --form 'trade_offering[id]=16'

#Response
{
  "message": "Not permited!"
}


#### GET Report - index
/api/v1/reports

#Response
{
  "percentage_survivors": {
    "percentage": 0
  },
  "percentage_non_infected": {
    "percentage": 0
  },
  "average_amount_items_by_survivor": {
    "water": 0,
    "food": 0,
    "medication": 0,
    "ammunition": 0
  },
  "points_lost_of_infected_survivor": {
    "lost_points": 0
  }
}

#### GET Report - percentage_survivors
/api/v1/reports/percentage_survivors

#Response
{
  "percentage_survivors": 0
}

#### GET Report - percentage_non_infected
/api/v1/reports/percentage_non_infected

#Response
{
  "percentage_non_infected": 0
}

#### GET Report - average_amount_items_by_survivor
/api/v1/reports/average_amount_items_by_survivor

#Response
{
  "water": 0,
  "food": 0,
  "medication": 0,
  "ammunition": 0
}

#### GET Report - points_lost_of_infected_survivor
/api/v1/reports/points_lost_of_infected_survivor

#Response
{
  "lost_points": 0
}

CODE    | BODY                            | DESCRIPTION
--------|---------------------------------|-----------
**200** | { 'message': 'ok' }             | When everything runs fine
**406** | { 'message': 'not_acceptable' } | When the phone number is invalid
**422** | { 'message': 'error' }          | Something went wrong or some required param is missing
