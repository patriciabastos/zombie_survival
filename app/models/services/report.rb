class Services::Report
  include ActionView::Helpers::NumberHelper

  class << self
    def percentage_survivors
      return 0 if all_survivors_count == 0
      percentage(infected_survivor_count / all_survivors_count)
    end

    def percentage_non_survivors
      return 0 if all_survivors_count == 0
      percentage(non_infected_survivor_count / all_survivors_count)
    end

    def water_by_survivor
      return 0 if non_infected_survivor_count == 0
      Inventory.water.count / non_infected_survivor_count
    end

    def food_by_survivor
      return 0 if non_infected_survivor_count == 0
      Inventory.food.count / non_infected_survivor_count
    end

    def medication_by_survivor
      return 0 if non_infected_survivor_count == 0
      Inventory.medication.count / non_infected_survivor_count
    end

    def ammunition_by_survivor
      return 0 if non_infected_survivor_count == 0
      Inventory.ammunition.count / non_infected_survivor_count
    end

    def percentage(value)
      value * 100
    end

    def lost_points
      Inventory.includes(:survivor).where(survivor_id: Survivor.infected).map(&:point).sum
    end

    def infected_survivor_count
      Survivor.infected.count
    end

    def all_survivors_count
      Survivor.all.count
    end

    def non_infected_survivor_count
      Survivor.non_infected.count
    end

    def average_amount_items_by_survivor
      Survivor.all
    end
  end
end