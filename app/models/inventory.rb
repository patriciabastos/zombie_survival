# == Schema Information
#
# Table name: inventories
#
#  id          :integer          not null, primary key
#  item        :string
#  survivor_id :integer          not null
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_inventories_on_survivor_id  (survivor_id)
#

class Inventory < ActiveRecord::Base
  belongs_to :survivor

  validates :item, presence: :true

  scope :water, -> { where(item: 'water') }
  scope :food, -> { where(item: 'food') }
  scope :medication, -> { where(item: 'medication') }
  scope :ammunition, -> { where(item: 'ammunition') }

  #name and points to item
  ITEMS = {
      water: 4,
      food: 3,
      medication: 2,
      ammunition: 1
    }

  def point
    Inventory::ITEMS[item.to_sym]
  end
end
