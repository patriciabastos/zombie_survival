# == Schema Information
#
# Table name: inventories
#
#  id          :integer          not null, primary key
#  item        :string
#  survivor_id :integer          not null
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_inventories_on_survivor_id  (survivor_id)
#

require 'rails_helper'

RSpec.describe Inventory, type: :model do
  context "validations" do
    it { is_expected.to validate_presence_of :item }
  end

  context "relationships" do
    it { is_expected.to belong_to(:survivor) }
  end
end
